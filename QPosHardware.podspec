Pod::Spec.new do |s|
  s.name             = 'QPosHardware'
  s.version          = '10.0.22'
  s.summary          = 'QPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-qpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "QPosHardware.xcframework"
  
  s.dependency 'Transactions', '~> 18.0', '< 18.1'
  s.swift_versions = '5.0'
end
